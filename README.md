### Добавление элементов (append)

Сравнительная таблица:

| Кол-во добавлений | SingleArray | VectorArray | FactorArray | MatrixArray | ArrayList |
|:-----------------:|:-----------:|:-----------:|:-----------:|:-----------:|:---------:|
| 2000              | 0.4187      | 0.0498      | 0.0052      | 0.0227      | 0.0018    |
| 5000              | 2.5889      | 0.2929      | 0.014       | 0.0582      | 0.0053    |
| 10000             | 10.230      | 1.139       | 0.028       | 0.1172      | 0.0088    |
| 100000            | -           | 110.3977    | 0.2699      | 1.2343      | 0.0881    |
| 1000000           | -           | -           | 2.6471      | 11.8592     | 0.869     |
| 10000000          | -           | -           | 27.5465     | -           | 8.847     |


_Время в таблице представлено в ***секундах***._


***Вывод:*** постоянное копирование в SingleArray очень сильно замедляет процесс добавления новых
элементов, в результате чего сложность добавления нового элемента ровняется O(n). В VectorArray 
дела обстоят чуть лучше, из-за запаса в 10 элементов, но при большом n запас в 10 элементов будет 
практически не ощутим, да и сложность алгоритма останется неизменной т.к. O(n/10) = O(n). 
В FactorArray из-за удвоения размена массива сложность составляет O(logn). Трудно сказать, но если судить
по таблице, то ArrayList (обертка над стандартными операциями c массивом в php) реализован очень близко к
FactorArray т.к. скорость их растет примерно одинаково. MatrixArray показал себя эффективней SingleArray 
и VectorArray за счет того, что не приходится каждый раз перекладывать элементы из старого массива в
новый, так же данная реализация менее требовательна к памяти, но её большой минус заключается в скорости
доступа к элементу и в скорости удаления элемента из массива.


### Вставка элементов (appendTo)
В данном тесте создается массив из N элементов, а затем вставляется элемент с индексом 0

Сравнительная таблица:

| Кол-во добавлений | SingleArray | VectorArray | FactorArray | MatrixArray | SpaceArray | ArrayList |
|:-----------------:|:-----------:|:-----------:|:-----------:|:-----------:|:----------:|:---------:|
| 5000              | 0.0012      | 0.0025      | 0.0013      | 0.0256      | 0.0004     | 0.0003    |
| 10000             | 0.0023      | 0.0049      | 0.0026      | 0.0522      | 0.0004     | 0.0001    |
| 100000            | -           | 0.0462      | 0.0246      | 0.5216      | 0.0004     | 0.0016    |
| 1000000           | -           | -           | 0.2466      | 5.4391      | 0.0004     | 0.0173    |
| 10000000          | -           | -           | 2.4838      | -           | 0.0006     | 0.2465    |

_Время в таблице представлено в ***секундах***._

### Удаление элемента (remove)
В данном тесте создается массив из N элементов, производится удаление элемента с индексом 0

Сравнительная таблица:

| Кол-во добавлений | SingleArray | VectorArray | FactorArray | MatrixArray | SpaceArray | ArrayList |
|:-----------------:|:-----------:|:-----------:|:-----------:|:-----------:|:----------:|:---------:|
| 5000              | 0.0012      | 0.0012      | 0.002       | 0.0329      | 0.0004     | 0.0001    |
| 10000             | 0.0022      | 0.0024      | 0.0039      | 0.0652      | 0.0004     | 0.0001    |
| 100000            | -           | 0.0232      | 0.0301      | 0.6588      | 0.0004     | 0.0011    |
| 1000000           | -           | -           | 0.245       | 7.1468      | 0.0004     | 0.0112    |
| 10000000          | -           | -           | 3.8551      | -           | 0.0007     | 0.119     |

_Время в таблице представлено в ***секундах***._
