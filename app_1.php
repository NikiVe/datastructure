<?php
include_once __DIR__ . '/vendor/autoload.php';

$a = new \DataStructure\SpaceArray(3);
$a->append(1);
$a->append(2);
$a->append(3);
$a->append(4);
$a->append(5);
$a->append(6);

$a->remove(0);
$a->remove(0);
$a->remove(0);

$a->append(1);
$a->appendTo(1, 13);

print_r($a);
exit();