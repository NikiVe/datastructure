<?php

namespace DataStructure;

use PHPUnit\Util\Exception;

class AbstructArray implements IArray
{
    protected array $array;
    protected int $length;

    public function __construct() {
        $this->array = [];
        $this->length = 0;
    }

    public function getLength(): int {
        return $this->length;
    }

    public function isEmpty(): bool {
        return $this->getLength() === 0;
    }

    public function append($item) {

    }

    public function get(int $n) {
        return $this->array[$n];
    }

    public function remove(int $n) {}

    public function appendTo(int $n, $item) {
        $length = $this->getLength();

        $newArray = array_fill(0, $this->getLength() + 1, null);
        for ($i = 0; $i < $n; $i++) {
            $newArray[$i] = $this->array[$i];
        }

        for ($i = $n; $i < $length; $i++) {
            $newArray[$i + 1] = $this->array[$i];
        }

        $newArray[$n] = $item;

        $this->array = $newArray;
        $this->length++;
    }

    public function getLast() {
        if ($this->getLength() === 0) {
            throw new \RuntimeException();
        }
        return $this->array[$this->getLength() - 1];
    }

    public function set(int $n, $item) {
        if ($n >= $this->getLength()) {
            throw new \RuntimeException();
        }
        $this->array[$n] = $item;
    }
}