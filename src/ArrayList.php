<?php


namespace DataStructure;


class ArrayList implements IArray
{
    protected $array;

    public function __construct() {
        $this->array = [];
    }

    public function getLength(): int {
        return count($this->array);
    }

    public function isEmpty(): bool {
        return empty($this->array);
    }

    public function append($item) {
        $this->array[] = $item;
        return $this;
    }

    public function get(int $n) {
        return $this->array[$n];
    }

    public function remove(int $n) {
        array_splice($this->array, $n, 1);
        return $this;
    }

    public function appendTo(int $n, $item) {
        array_splice($this->array, $n, 0, $item);
        return $this;
    }

    public function getLast()
    {
        // TODO: Implement getLast() method.
    }

    public function set(int $n, $item) {
        $this->array[$n] = $item;
        return $this;
    }
}