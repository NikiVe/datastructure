<?php


namespace DataStructure;


class FactorArray extends AbstructArray
{
    protected int $size;
    protected int $factor;

    public function __construct() {
        parent::__construct();
        $this->size   = 8;
        $this->array = array_fill(0, $this->size, null);
        $this->factor = 2;
    }

    public function append($item) {
        if ($this->getLength() === $this->size) {
            $this->resize();
        }

        $this->array[$this->getLength()] = $item;
        $this->length++;
    }

    protected function resize() {
        $newSize = $this->size * $this->factor;
        $newArray = array_fill(0, $newSize, null); // как бы создание массива

        if ($this->size >= 0) {
            for ($i = 0; $i < $this->size; $i++) {
                $newArray[$i] = $this->array[$i];
            }
        }
        $this->size = $newSize;
        $this->array = $newArray;
    }

    public function remove(int $n) {
        if ($n < 0 || $this->getLength() === 0) {
            throw new \Exception();
        }

        $result = $this->array[$n];

        $newArray = array_fill(0, $this->getLength() - 1, null);;

        for ($i = 0; $i < $n; $i++) {
            $newArray[$i] = $this->array[$i];
        }
        for ($i = $n + 1; $i < $this->size; $i++) {
            $newArray[$i - 1] = $this->array[$i];
        }

        $this->array = $newArray;
        $this->length--;
        $this->size--;

        return $result;
    }

    public function appendTo(int $n, $item) {
        $length = $this->getLength();
        if ($length === $this->size) {
            $this->resize();
        }

        for ($i = $length; $i >= $n; $i--) {
            $this->array[$i + 1] = $this->array[$i];
        }

        $this->array[$n] = $item;
        $this->length++;
    }
}