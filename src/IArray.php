<?php


namespace DataStructure;


Interface IArray
{
    public function getLength() : int;

    public function isEmpty() : bool;

    public function append($item);

    public function get(int $n);

    public function remove(int $n);

    /**
     * Вставить элемент в указанную позицию
     *
     * @param int $n
     * @param $item
     *
     * @return mixed
     */
    public function appendTo(int $n, $item);

    public function getLast();

    public function set(int $n, $item);
}