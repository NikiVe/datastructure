<?php


namespace DataStructure;


class Main
{
    public static function main() {
        //self::testRemove(new SingleArray(), 10000);
        //self::testRemove(new VectorArray(), 100000);
        //self::testRemove(new FactorArray(), 10000000);
        //self::testRemove(new MatrixArray(), 1000000);
        //self::testRemove(new SpaceArray(), 10000000);
        //self::testRemove(new ArrayList(), 10000000);
    }

    private static function testAppend(IArray $array, int $total) {
        $sT = microtime(true);

        for ($j = 0; $j < $total; $j++) {
            $array->append($j);
        }

        echo get_class($array) . ': ';
        echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;
    }

    private static function testAppendTo(IArray $array, int $total) {
        for ($j = 0; $j < $total; $j++) {
            $array->append($j);
        }

        $sT = microtime(true);
        $array->appendTo(0, 0);
        echo get_class($array) . ': ';
        echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;
    }

    private static function testRemove(IArray $array, int $total) {
        for ($j = 0; $j < $total; $j++) {
            $array->append($j);
        }

        $sT = microtime(true);
        $array->remove(0);
        echo get_class($array) . ': ';
        echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;
    }
}