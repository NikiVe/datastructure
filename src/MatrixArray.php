<?php


namespace DataStructure;


class MatrixArray implements IArray
{
    protected int $length;
    protected int $sizeLine;
    protected FactorArray $box;

    public function __construct($sizeLine = 2) {
        $this->length = 0;
        $this->box = new FactorArray();
        $this->sizeLine = $sizeLine;
    }

    public function getLength(): int {
        return $this->length;
    }

    public function isEmpty(): bool {
        return $this->length === 0;
    }

    public function append($item) {
        if ($this->length === $this->box->getLength() * $this->sizeLine) {
            $this->resize();
        }
        $this->box->get($this->length / $this->sizeLine)->append($item);
        $this->length++;
    }

    public function resize() {
        $this->box->append(new VectorArray($this->sizeLine));
    }

    public function get(int $n) {
        return $this->box->get((int)floor($n / $this->sizeLine))->get($n % $this->sizeLine);
    }

    public function remove(int $n) {
        if ($n < 0 || $this->getLength() === 0) {
            throw new \Exception();
        }

        $line = (int)floor($n / $this->sizeLine);

        $result = $this->box->get($line)->remove($n % $this->sizeLine);

        $boxSize = $this->box->getLength();
        for ($i = $line; $i < $boxSize - 1; $i++) {
            $this->box->get($i)->append($this->box->get($i + 1)->remove(0));
        }

        if ($this->box->get($boxSize - 1)->getLength() === 0) {
            $this->box->remove($boxSize - 1);
        }
        $this->length--;
        return $result;
    }

    public function getLast() {
        return $this->box->getLast()->getLast();
    }

    public function set(int $n, $item) {
        if ($n >= $this->getLength()) {
            throw new \RuntimeException();
        }
        $this->box->get((int)floor($n / $this->sizeLine))->set($n % $this->sizeLine, $item);
    }

    public function appendTo(int $n, $item) {
        if ($n >= $this->getLength()) {
            throw new \RuntimeException();
        }
        $line = (int)floor($n / $this->sizeLine);
        $p = $n % $this->sizeLine;
        $startLine = $this->box->getLength() - 1;

        $this->append($this->box->getLast()->getLast());

        for ($l = $startLine; $l > $line; $l--) {
            $this->lineShift($l);
            if ($l > 0) {
                $array = $this->box->get($l - 1);
                $last = $array->getLast();
                $this->box->get($l)->set(0, $last);
            }
        }

        $this->lineShift($line, $p);
        $this->box->get($line)->set($p, $item);

        $this->length++;
    }

    protected function lineShift(int $line, $sP = 0) {
        $array = $this->box->get($line);
        for ($i = $array->getLength() - 1; $i > $sP; $i--) {
            $array->set($i, $array->get($i - 1));
        }
    }
}