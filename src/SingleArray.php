<?php

namespace DataStructure;

class SingleArray extends AbstructArray
{
    public function append($item) {
        $this->resize();
        $this->array[$this->getLength() - 1] = $item;
    }

    protected function resize() {
        $length = $this->getLength();
        $newArray = array_fill(0,$length + 1, null); // как бы создание массива

        if ($length >= 0) {
            for ($i = 0; $i < $length; $i++) {
                $newArray[$i] = $this->array[$i];
            }
        }
        $this->array = $newArray;
        $this->length++;
    }

    public function remove(int $n) {
        if ($n < 0 || $this->getLength() === 0) {
            throw new \Exception();
        }

        $length = $this->getLength();
        $result = $this->array[$n];

        $newArray = array_fill(0, $this->getLength() - 1, null);;

        for ($i = 0; $i < $n; $i++) {
            $newArray[$i] = $this->array[$i];
        }
        for ($i = $n + 1; $i < $length; $i++) {
            $newArray[$i - 1] = $this->array[$i];
        }

        $this->array = $newArray;
        $this->length--;

        return $result;
    }
}