<?php


namespace DataStructure;


use DataStructure\Struct\ArrayPos;

class SpaceArray extends MatrixArray
{
    public function __construct($sizeLine = 2) {
        parent::__construct($sizeLine);
        $this->box->append(new VectorArrayForSpace($this->sizeLine));
    }

    public function resize() {
        $this->box->append(new VectorArrayForSpace($this->sizeLine));
    }

    public function append($item) {
        if ($this->box->getLast()->getLength() === $this->sizeLine) {
            $this->resize();
        }
        $this->box->getLast()->append($item);

        $this->length++;
    }

    public function appendTo(int $n, $item) {
        $arPos = $this->getArrayAndPositionByPosition($n);

        $arPos->array->appendTo($arPos->position, $item);
    }

    public function remove(int $n) {
        $arPos = $this->getArrayAndPositionByPosition($n);
        $result = $arPos->array->remove($arPos->position);
        if ($arPos->array->getLength() === 0) {
            $this->box->remove($arPos->floor);
        }
        return $result;
    }

    protected function getArrayAndPositionByPosition(int $p) {
        $floorCount = $this->box->getLength();
        $curLength = 0;

        for ($i = 0; $i < $floorCount; $i++) {
            $arrayTemp = $this->box->get($i);
            if ($curLength + $arrayTemp->getLength() > $p) {
                return new ArrayPos($arrayTemp, $p - $curLength, $i);
            }

            $curLength += $arrayTemp->getLength();
        }

        throw new \RuntimeException();
    }

    public function get(int $n) {
        $arPos = $this->getArrayAndPositionByPosition($n);
        return $arPos->array->get($arPos->position);
    }

    public function set(int $n, $item) {
        if ($n >= $this->getLength()) {
            throw new \RuntimeException();
        }
        $arPos = $this->getArrayAndPositionByPosition($n);
        $arPos->array->set($arPos->position, $item);
    }
}