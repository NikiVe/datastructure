<?php


namespace DataStructure\Struct;


use DataStructure\IArray;

class ArrayPos
{
    public IArray $array;
    public int $position;
    public int $floor;

    public function __construct(IArray $array, int $position, int $floor) {
        $this->array = $array;
        $this->position = $position;
        $this->floor = $floor;
    }
}