<?php


namespace DataStructure;


class VectorArrayForSpace extends VectorArray
{
    protected function resize() {
        $newSize = $this->size === 0 ? $this->size + $this->vector * 2 : $this->size + $this->vector;
        $newArray = array_fill(0, $newSize, null); // как бы создание массива

        if ($this->size >= 0) {
            for ($i = 0; $i < $this->size; $i++) {
                $newArray[$i] = $this->array[$i];
            }
        }
        $this->size = $newSize;
        $this->array = $newArray;
    }
}